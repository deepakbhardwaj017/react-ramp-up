import {
  GET_ARTICLES_REQUEST, GET_ARTICLE_DETAIL_REQUEST, SIGN_IN_REQUEST, SIGN_UP_REQUEST, LOGOUT_REQUEST
} from '../actions';

let defaultState = {
  user:{
    username: "",
    email: "",
    token: ""
  },
  articles:[]
};

export default (state=defaultState, action) => {
  switch (action.type) {
    case GET_ARTICLES_REQUEST:
      return {
        ...state,
        articles: action.articles,
      };
    // case GET_ARTICLE_DETAIL_REQUEST:
    //   return {
    //     ...state
    //   };
    case SIGN_IN_REQUEST:
      return {
        ...state,
        user: {
          username: action.user.user.username,
          email: action.user.user.email,
          token: action.user.user.token
        }
      };
    case SIGN_UP_REQUEST:
      return {
        ...state,
        user: {
          username: action.user.user.username,
          email: action.user.user.email,
          token: action.user.user.token
        }
      };
    case LOGOUT_REQUEST:
      return {
        ...state,
        user: {
          username: "",
          email: "",
          token: ""
        }
      };
    default:
      return state;
  }
};
