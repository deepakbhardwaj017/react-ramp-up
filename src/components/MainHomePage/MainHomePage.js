import React, {Component} from 'react';
import {connect} from "react-redux";
import { Link } from 'react-router';

class MainHomePage extends Component{
  render() {
    console.log("{this.props}", this.props);
    return (
        <div>
            <div>
                <Link to="/sign-in" onlyActiveOnIndex>SignIn</Link>
            </div>
            <div>
                <Link to="/sign-up" onlyActiveOnIndex>SignUp</Link>
            </div>
        </div>
    )
  }
}

const mapStateToProps = (state, ownProps) => ({
  todo: state
});

export default connect(mapStateToProps, {})(MainHomePage);