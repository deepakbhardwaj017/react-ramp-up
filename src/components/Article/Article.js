import React, {Component} from 'react';
// import {connect} from "react-redux";
import { Link } from 'react-router';

class Article extends Component{
    render() {
        console.log("{this.props}", this.props);
        const {slug, title} = this.props;
        return (
            <div>
                <div>
                    <Link to="/articles/:slug" onlyActiveOnIndex>{title}</Link>
                </div>
            </div>
        )
    }
}

export default Article;
