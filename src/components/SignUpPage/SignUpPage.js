import React, {Component} from "react";
// import {browserHistory} from "react-router";
import {connect} from "react-redux";
import {signUpUser} from "../../actions";

class SignUpPage extends Component{
  state = {
    user: {
      username:"",
      email:"",
      password:""
    },
    // token: "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6Nzk3NzYsInVzZXJuYW1lIjoiS0tLLktLayIsImV4cCI6MTU4MzQ5NDUxMH0.QvsSOLXTx8GBhPzM2fUNxW0oXGMmAKyGxsupQK4TpYQ"
  };

  onSubmit(user) {
    const {signUpUser,history} = this.props;
    console.log("done!92!signUpUser", this.props);
    signUpUser({
      data:user,
      onSuccess:(res)=>{
        console.log("SignUpuser success :-",res);
        history.push("/sign-up");
      },
      onError:(err)=>{
        console.log("SignUpuser error :-",err)
      }
    });
    // history.push('/sign-up')
  }

  setName(username){
    this.setState({ user: { ...this.state.user, username: username } });
  }

  setEmail(email){
    this.setState({ user: { ...this.state.user, email: email } });
  }

  setPassword(password){
    this.setState({ user: { ...this.state.user, password: password } });
  }

  render(){
    console.log("SignUpPage Props", this.props);
    return (
        <div>
          <div>
            <label> NAME : </label>
            <input name="email" value={this.state.user.username} onChange={e => this.setName(e.target.value)}/>
          </div>
          <div>
            <label> EMAIL : </label>
            <input name="email" value={this.state.user.email} onChange={e => this.setEmail(e.target.value)}/>
          </div>
          <div>
            <label> PASSWORD : </label>
            <input name="password" value={this.state.user.password} onChange={e => this.setPassword(e.target.value)}/>
          </div>
          <button type="button" onClick={() => this.onSubmit(this.state)}>Submit</button>
        </div>
    )
  }
}

const mapStateToProps = (state, ownProps) => ({
  state: state
});

export default connect(mapStateToProps, { signUpUser })(SignUpPage);