import React, { Component, useState } from 'react';
import { connect } from 'react-redux';
import { signInUser } from '../../actions';
// import PropTypes from 'prop-types';
// import {PostPage} from "../PostPage/PostPage";
// import {Link} from "react-router";
import { Router, browserHistory } from 'react-router';
// import history from '../../index'

class SignInPage extends Component{
    state = {
        user: {
            email:"",
            password:""
        },
        token: "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6Nzk3NzYsInVzZXJuYW1lIjoiS0tLLktLayIsImV4cCI6MTU4MzQ5NDUxMH0.QvsSOLXTx8GBhPzM2fUNxW0oXGMmAKyGxsupQK4TpYQ"
    };

    onSubmit(user) {
        const {signInUser,history} = this.props;
        console.log("done!92", this.props);
        signInUser({
            data:user,
            onSuccess:(res)=>{
                console.log("SignInuser sucess :-",res);
                browserHistory.push("/sign-up");
            },
            onError:(err)=>{
                console.log("SignInuser error :-",err)
            }
        });
        history.push('/sign-up')
    }

    setEmail(email){
        this.setState({ user: { ...this.state.user, email: email } });
    }

    setPassword(password){
        this.setState({ user: { ...this.state.user, password: password } });
    }

    render(){
        console.log("SignInPage Props", this.props);
        return (
            <div>
                <div>
                    <label> EMAIL : </label>
                    <input name="email" value={this.state.user.email} onChange={e => this.setEmail(e.target.value)}/>
                </div>
                <div>
                    <label> PASSWORD : </label>
                    <input name="password" value={this.state.user.password} onChange={e => this.setPassword(e.target.value)}/>
                </div>
                <button type="button" onClick={() => this.onSubmit(this.state)}>Submit</button>
            </div>
        )
    }
}

const mapStateToProps = (state, ownProps) => ({
    state: state
});

export default connect(mapStateToProps, { signInUser })(SignInPage);
