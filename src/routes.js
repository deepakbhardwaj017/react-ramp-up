import React from 'react';
import {Router,  Route, IndexRoute, Switch} from 'react-router';

import MainHomePage from './components/MainHomePage/MainHomePage';
import SignInPage from './components/SignInPage/SignInPage';
import SignUpPage from './components/SignUpPage/SignUpPage';
import ArticlesListPage from './components/ArticlesListPage/ArticlesListPage';
import ArticleDetailPage from './components/ArticleDetailPage/ArticleDetailPage';

export default (
    <div>
      <Route exact path="/" component={MainHomePage}/>
      {/*<IndexRoute component={HomePage} />*/}
      <Route exact path="/sign-in" component={SignInPage} />
      <Route exact path="/sign-up" component={SignUpPage} />
      <Route path="/articles" component={ArticlesListPage} />
      <Route path="/articles/:slug" component={ArticleDetailPage} />
      {/*<Route path="/post/:postID" component={PostPage} />*/}
    </div>
);
