// import api from '../lib/api';
import axios from 'axios';
// import Cookie from 'js.cookie'
import cookie from 'react-cookies'

if(cookie.load('token')){
    axios.defaults.headers.common['Authorization'] = cookie.load('token');
}

// function addAuthorizationHeader() {
//     // if (cookie.load('token')) {
//     //     axios.defaults.headers.common['Authorization'] = cookie.load('token');
//     // }
//     axios.defaults.headers.common['Authorization'] = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6Nzk3NzYsInVzZXJuYW1lIjoiS0tLLktLayIsImV4cCI6MTU4MzQ5NDUxMH0.QvsSOLXTx8GBhPzM2fUNxW0oXGMmAKyGxsupQK4TpYQ"
// }

export const GET_ARTICLES_REQUEST = 'GET_ARTICLES_REQUEST';
export const GET_ARTICLE_DETAIL_REQUEST = 'GET_ARTICLE_DETAIL_REQUEST';
export const SIGN_IN_REQUEST = 'SIGN_IN_REQUEST';
export const SIGN_UP_REQUEST = 'SIGN_UP_REQUEST';
export const LOGOUT_REQUEST = 'LOGOUT_REQUEST';

export const getArticles = json => ({
  type: GET_ARTICLES_REQUEST,
  articles: json.data || []
});

export const getArticleDetail = json => ({
  type: GET_ARTICLE_DETAIL_REQUEST,
  article: json.data || []
});

export const signIn = json => ({
  type: SIGN_IN_REQUEST,
  user: json.data
});

export const signUp = json => ({
  type: SIGN_UP_REQUEST,
  user: json.data
});

export const logout = () => ({
  type: LOGOUT_REQUEST
});

export function fetchArticles({data,onSuccess,onError})
{
    // addAuthorizationHeader();
    return (dispatch => axios({
            method: "POST",
            url: "https://conduit.productionready.io/api/articles?limit=20",
            headers: {},
            data: data
        })
            .then(json => {
                onSuccess && onSuccess(json);
                dispatch(signIn(json))
            })
            .catch(err => {
                onError && onError(err);
            })
    );
}

export const fetchArticleDetail = () => (
    dispatch => axios('https://jsonplaceholder.typicode.com/posts')
    .then(
        json => dispatch(getArticleDetail(json)),
    )
);

export function signInUser({data,onSuccess,onError})
{
    // addAuthorizationHeader();
    return (dispatch => axios({
            method: "POST",
            url: "https://conduit.productionready.io/api/users/login",
            headers: {},
            data: data
        })
            .then(json => {
                onSuccess && onSuccess(json);
                dispatch(signIn(json))
            })
            .catch(err => {
                onError && onError(err);
            })
    );
}

export function signUpUser({data,onSuccess,onError})
{
    // addAuthorizationHeader();
    return (dispatch => axios({
            method: "POST",
            url: "https://conduit.productionready.io/api/users",
            headers: {},
            data: data
        })
            .then(json => {
                onSuccess && onSuccess(json);
                dispatch(signUp(json))
            })
            .catch(err => {
                onError && onError(err);
            })
    );
}

export const logoutUser = () => (
    dispatch => axios('https://jsonplaceholder.typicode.com/posts')
        .then(
            json => dispatch(logoutUser(json)),
        )
);
